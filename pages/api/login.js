import API from './../../helpers/api'
import Cookies from 'cookies'

async function handler (req, res) {
  if (req.method !== 'POST') return res.status(404)

  const cookies = new Cookies(req, res)
  const api = new API()

  const { email, password } = JSON.parse(req.body || '{}')

  const [user = {}, loginError] = await api.login(email, password)
  if (loginError) {
    console.error(loginError)
    return res.status(401).json(loginError)
  }

  cookies.set('AUTH_TOKEN', user.access_token)

  // Return the profile and user information
  return res.json({
    data: { user }
  })
}

export default handler
